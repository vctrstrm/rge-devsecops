---
- name: Local Post-Install Config (secrets, etc)
  hosts: bastion
  gather_facts: true
  vars:
    remote_kubeconfig_path: /home/elastic/.kube/config
    local_kubeconfig_name: "{{ deployment_domain }}.kubeconfig"
    local_secrets_dir: "{{ playbook_dir }}/../generated/secrets/"
    k8s_hostname: "{{ deployment_domain }}"

  tasks:
    - name: Gathering Secrets
      block:
        - ansible.builtin.tempfile:
            state: directory
            prefix: fetch_tmpdir_
          register: fetch_tmpdir
        - name: Preparing the kubeconfig file
          ansible.builtin.shell:
            cmd: |
              set -o nounset -o errexit -o pipefail
              cp "{{ remote_kubeconfig_path }}" "{{ tmp_kubeconfig_path }}"
              kubectl config --kubeconfig {{ tmp_kubeconfig_path }} set-cluster default --server=https://{{ k8s_hostname }}:6443
          vars:
            tmp_kubeconfig_path: "{{ (fetch_tmpdir.path, local_kubeconfig_name) | path_join }}"
        - name: Preparing the rancher bootstrap secret
          vars:
            tmp_rancher_pass_path: >-
              {{ (fetch_tmpdir.path, "rancher." + deployment_domain + ".bootpass") | path_join }}
          ansible.builtin.shell:
            cmd: |
              set -o nounset -o errexit -o pipefail
              kubectl get secret --namespace cattle-system bootstrap-secret -o {%raw%}go-template='{{ .data.bootstrapPassword|base64decode }}'{%endraw%} > "{{ tmp_rancher_pass_path }}"
        - name: Capturing all files staged for transfer
          ansible.builtin.find:
            paths: "{{ fetch_tmpdir.path }}"
            file_type: file
          register: staged_files
        - name: Fetching files locally
          ansible.builtin.fetch:
            src: "{{ item.path }}"
            dest: "{{ local_secrets_dir }}/"
            flat: true
          loop: "{{ staged_files.files }}"
      always:
        - ansible.builtin.file:
            path: "{{ fetch_tmpdir.path }}"
            state: absent
    - name: Installing Kubeconfig
      delegate_to: localhost
      run_once: true
      block:
        - name: Confirm Replacing Kubeconfig
          ansible.builtin.pause:
            prompt: Do you want to install the kubeconfig to ~/.kube/config? [Enter] to continue, [Ctrl+C then A] to abort
        - name: Copying the new kubeconfig
          ansible.builtin.shell:
            executable: bash
            cmd: |
              set -o nounset -o errexit -o pipefail

              mkdir -p "{{ kubeconfig_path | dirname }}"
              cp "{{ new_kubeconfig_path }}" "{{ kubeconfig_path }}"
              chmod 600 "{{ kubeconfig_path }}"
          vars:
            new_kubeconfig_path: "{{ (local_secrets_dir, local_kubeconfig_name) | path_join }}"
            kubeconfig_path: "{{ (lookup('env','HOME'), '.kube', 'config') | path_join }}"
