# Ansible

## The list of videos specific to this topic, repeats from the main page
1. [What is Ansible in 3 minutes](https://youtu.be/tWR1KXgEYxE)

## A few words about ansible...

A lot happens in these ansible playbooks and roles, but they should be structured enough that you can take a look at playbooks and understand rough configuration steps. - The YAML files at the root of `<repo>/ansible` directory are the Ansible playbooks.

The ansible playbooks will reference various roles. You can, and probably should, also look at the ansible YAML files behind these roles in the `<repo>/ansible/roles` directory. This will go a long way towards helping you understand what's going on and what may be going wrong (should you be so unlucky).
## Step 1: Execute the playbook to setup local environment

Before running any other ansible playbooks, we need to configure our local environment to make sure our SSH configs & DNS aliases are in place.

Run the following, from the `<repo>/ansible/` directory:
- `ansible-playbook -K local-pre-install.yml`

## Step 2: Install Rancher k8s

To install rancher kubernetes, run the `setup-rancher.yml` playbook, e.g. `ansible-playbook setup-rancher.yml`

## Step 3: Acquire configs/secrets to complete setup and use the env

To finish bootstrap process and use kubectl utils, you're gonna need some credentials and passwords. Run the `local-post-install.yml` playbook to acquire those secrets; look for them in `<repo>/generated/secrets` directory.

