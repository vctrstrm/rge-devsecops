#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#####
## ## Docs:
#####   - https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-helm/
  ###
   ##
    #

# install conf
LOCAL_HELM_REPO_NAME="nginx-stable"
REMOTE_HELM_REPO_URL="https://helm.nginx.com/stable"
CHART_NAME="$LOCAL_HELM_REPO_NAME/nginx-ingress"
RELEASE_NAME="nginx-ingress"
NAMESPACE="ingress-operator"
ENV_VALUES="$SCRIPT_DIR/local.values.yaml"

# helm repo setup
helm repo add "$LOCAL_HELM_REPO_NAME" "$REMOTE_HELM_REPO_URL"
helm repo update

# install chart
helm upgrade --install \
    "$RELEASE_NAME" "$CHART_NAME" \
    --namespace "$NAMESPACE" \
    --create-namespace \
    --values "$ENV_VALUES"