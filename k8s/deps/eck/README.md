# Install ECK in the K8 Cluster
These instructions are following the [Getting Started Guide](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-deploy-eck.html) in the Elastic docs. For better understanding it might be good to read the instructoins directly from that webpage. For simplicity they are added below to get an Elastic cluster up and running

## Step 1: Deploy ECK in your K8 Cluster
### Step 1.1: Install custom resource definitions:

Now that kubectl is sync'd to Rancher, on your personal computer run the below command. All commands in this README should be run on the personal computer
```
kubectl create -f https://download.elastic.co/downloads/eck/2.4.0/crds.yaml
```


This creates the following Elastic resources have been created as seen in the CLI:
- customresourcedefinition.apiextensions.k8s.io/agents.agent.k8s.elastic.co created
- customresourcedefinition.apiextensions.k8s.io/apmservers.apm.k8s.elastic.co created
- customresourcedefinition.apiextensions.k8s.io/beats.beat.k8s.elastic.co created
- customresourcedefinition.apiextensions.k8s.io/elasticmapsservers.maps.k8s.elastic.co created
- customresourcedefinition.apiextensions.k8s.io/elasticsearches.elasticsearch.k8s.elastic.co created
- customresourcedefinition.apiextensions.k8s.io/enterprisesearches.enterprisesearch.k8s.elastic.co created
- customresourcedefinition.apiextensions.k8s.io/kibanas.kibana.k8s.elastic.co created


### Step 1.2: Install the operator with its RBAC rules
Run the below command:
```
kubectl apply -f https://download.elastic.co/downloads/eck/2.4.0/operator.yaml
```

### Step 1.3: 
Monitor the operator logs wit hthe below command:
```
kubectl -n elastic-system logs -f statefulset.apps/elastic-operator
```

## Step 2: Deploy an Elastic Cluster
Apply a simple Elasticsearch cluster specification, with one Elasticsearch node:

### Step 2.1 Build the Manifest file
Run the below command. The operator automatically creates and manages Kubernetes resources to achieve the desired state of the Elasticsearch cluster. It may take up to a few minutes until all the resources are created and the cluster is ready for use.
```
cat <<EOF | kubectl apply -f -
apiVersion: elasticsearch.k8s.elastic.co/v1
kind: Elasticsearch
metadata:
  name: quickstart
spec:
  version: 8.4.1
  nodeSets:
  - name: default
    count: 1
    config:
      node.store.allow_mmap: false
EOF
```

### Step 2.2: Monitor cluster health and creation progress
Get an overview of the current Elasticsearch clusters in the Kubernetes cluster, including health, version and number of nodes:
```
kubectl get elasticsearch 
```

The CLI should show the **NAME HEALTH NODES ..** of these pods. When you create the cluster, there is no HEALTH status and the PHASE is empty. After a while, the PHASE turns into Ready, and HEALTH becomes green. The HEALTH status comes from Elasticsearch’s cluster health API.


### Step 2.3: Request Elasticsearch access
A [ClusterIP Service](https://kubernetes.io/docs/concepts/services-networking/service/) is automatically created for your cluster. The below command will show you info about it. We are going to be able to ping it from our personal computer via port-forwarding
```
kubectl get service quickstart-es-http 
```

#### Step 2.3.1: Get the credentials
A default user named elastic is automatically created with the password stored in a Kubernetes secret:
```
PASSWORD=$(kubectl get secret quickstart-es-elastic-user -o go-template='{{.data.elastic | base64decode}}')
```
#### Step 2.3.2: Request the Elasticsearch node
Use the following command **in a separate terminal**:
```
kubectl port-forward service/quickstart-es-http 9200
```

#### Step 2.3.3: Curl the Elastic node
Run this command in the original terminal to touch the elasticsearch node. You should receive a response from the Elasticsearch saying "tagline" : "You Know, for Search"
```
curl -u "elastic:$PASSWORD" -k "https://localhost:9200"
```

## Step 3: Deploy a Kibana Instance
To deploy your Kibana instance go through the following steps.

### Step 3.1: Create the Manifest and Apply it
Specify a Kibana instance and associate it with your Elasticsearch cluster:
```
cat <<EOF | kubectl apply -f -
apiVersion: kibana.k8s.elastic.co/v1
kind: Kibana
metadata:
  name: quickstart
spec:
  version: 8.4.1
  count: 1
  elasticsearchRef:
    name: quickstart
EOF
```

### Step 3.2: Monitor Kibana health and creation progress
Similar to Elasticsearch, you can retrieve details about Kibana instances:
```
kubectl get pod --selector='kibana.k8s.elastic.co/name=quickstart'
```


### Step 3.3: Access Kibana

Like the Elasticsearch pod, A [ClusterIP Service](https://kubernetes.io/docs/concepts/services-networking/service/) is automatically created for Kibana:

#### Step 3.3.1: Use kubectl port-forward to access Kibana from your local workstation:
Run the following command
```
kubectl port-forward service/quickstart-kb-http 5601
```

#### Step 3.3.2: Check it on Browser
Open https://localhost:5601 in your browser. Your browser will show a warning because the self-signed certificate configured by default is not verified by a known certificate authority and not trusted by your browser

#### Step 3.3.3: Login
Login as the *elastic* user. The password can be obtained with the following command:
```
kubectl get secret quickstart-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo
```






