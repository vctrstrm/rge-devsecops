#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

git config --global user.name "Elastic"
git config --global user.email "elastic@elastic.local"
git config --global credential.helper store
git config --global http.sslVerify false