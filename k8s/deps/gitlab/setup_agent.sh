#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

set -x

#####
## ## Docs:
#####   - https://docs.gitlab.com/charts/installation/deployment.html
  ###
   ##
    #

# install conf
LOCAL_HELM_REPO_NAME="gitlab"
REMOTE_HELM_REPO_URL="https://charts.gitlab.io"
CHART_NAME="$LOCAL_HELM_REPO_NAME/gitlab-agent"
RELEASE_NAME="gitlab-agent"
NAMESPACE="gitlab-agent-test"
ENV_VALUES="$SCRIPT_DIR/agent.values.yaml"

# setup helm
helm repo add "$LOCAL_HELM_REPO_NAME" "$REMOTE_HELM_REPO_URL"
helm repo update

_ca_crt="$(kubectl get secrets -n gitlab gitlab-wildcard-tls-chain -o go-template='{{ index .data "gitlab.aws.estorm-eck-lab.local.crt" | base64decode }}')"

# install
helm upgrade --install \
    "$RELEASE_NAME" "$CHART_NAME" \
    --namespace "$NAMESPACE" \
    --create-namespace \
    --set image.tag=v15.8.0 \
    --set config.token="${GITLAB_AGENT_TOKEN:?Specify the agent registration token}" \
    --set config.kasAddress=wss://kas.aws.estorm-eck-lab.local \
    --set config.caCert="$_ca_crt"