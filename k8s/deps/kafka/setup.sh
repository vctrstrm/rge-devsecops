#!/usr/bin/env bash
set -o nounset -o errexit -o pipefail
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#####
## ## Docs:
#####   - https://docs.confluent.io/operator/current/co-quickstart.html
  ###
   ##
    #

# install conf
LOCAL_HELM_REPO_NAME="confluent"
REMOTE_HELM_REPO_URL="https://packages.confluent.io/helm"
CHART_NAME="$LOCAL_HELM_REPO_NAME/confluent-for-kubernetes"
RELEASE_NAME="confluent-operator"
NAMESPACE="confluent"
ENV_VALUES="$SCRIPT_DIR/local.values.yaml"

# setup helm
helm repo add "$LOCAL_HELM_REPO_NAME" "$REMOTE_HELM_REPO_URL"
helm repo update

# install
helm upgrade --install \
    "$RELEASE_NAME" "$CHART_NAME" \
    --namespace "$NAMESPACE" \
    --create-namespace \
    --values "$ENV_VALUES"