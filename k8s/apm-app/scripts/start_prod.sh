#!/usr/bin/env bash
set -CEfue
set -o pipefail

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
APP_ROOT_DIR=$( cd "${SCRIPT_DIR}/../" &> /dev/null && pwd )
APP_SRC_DIR="${APP_ROOT_DIR}/src"

VENV_DIR="${APP_ROOT_DIR}/venv"
VENV_PYTHON_CMD="${VENV_DIR}/bin/python"

export ELATIC_APM_ENABLED="${ELATIC_APM_ENABLED:-False}"
export ELASTIC_APM_VERIFY_SERVER_CERT="${ELASTIC_APM_VERIFY_SERVER_CERT:-False}"
export ELASTIC_APM_SERVICE_NAME="${ELASTIC_APM_SERVICE_NAME:-example-app}"
export ELASTIC_APM_SERVER_URL="${ELASTIC_APM_SERVER_URL:-http://localhost:8200}"

APP_HOST=${APP_HOST:-0.0.0.0}
APP_PORT=${APP_PORT:-5000}

env PYTHONPATH="${PYTHONPATH:-}${PYTHONPATH:+:}${APP_SRC_DIR}" \
    "$VENV_PYTHON_CMD" -m gunicorn --bind "${APP_HOST}:${APP_PORT}" "app.main:app"