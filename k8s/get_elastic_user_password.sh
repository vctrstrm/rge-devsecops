#!/usr/bin/env bash

kubectl get secrets elastic-es-es-elastic-user -o go-template='{{ .data.elastic | base64decode | printf "%s\n" }}'
