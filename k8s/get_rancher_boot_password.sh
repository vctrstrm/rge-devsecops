#!/usr/bin/env bash

kubectl get secret -n cattle-system bootstrap-secret -o go-template='{{ .data.bootstrapPassword | base64decode | printf "%s\n" }}'