#####
## ## These are the variables that always need to be specified by users (with minor exceptions)
#####  - the idea is to keep the set of these very small to simplify the user experience

variable "deployment_owner" {
  description = "Name of the owner for this deployment"
  type        = string

  validation {
    condition     = length(var.deployment_owner) >= 1
    error_message = "[ERROR] Make sure to set the deployment_owner!"
  }
}

variable "deployment_email" {
  description = "Name of this deployment"
  type        = string

  validation {
    condition     = can(regex("^\\S+[@]\\S+[.]\\S+$", var.deployment_email))
    error_message = "[ERROR] Unexpected email format"
  }
}

variable "deployment_prefix" {
  description = "Prefix used to tag resources part of this deployment"
  type        = string

  validation {
    condition     = can(regex("^[a-z][a-z-]{4,}[a-z]$", var.deployment_prefix))
    error_message = "[ERROR] Deployment prefix should be at least 6 lowercase letters, with optional dashes."
  }
}

variable "deployment_location" {
  description = "Shortname for the deployment location/region"
  type        = string
  default     = "us-west"
}

variable "ssh_key_path" {
  description = "Path to the SSH private key"
  type        = string

  default = "~/.ssh/id_rsa"

  validation {
    condition     = fileexists(var.ssh_key_path)
    error_message = "[ERROR] The given file path is invalid: ${var.ssh_key_path}"
  }
}

variable "deploy_managed_k8s" {
  description = "[WIP] Toggle to deploy managed k8s"
  type        = bool
  default     = false

  validation {
    condition     = var.deploy_managed_k8s == false
    error_message = "[ERROR] Managed k8s not yet supported"
  }
}

// Elastic org-specific required tags
variable "elastic_deployment_tags" {
  description = "Required deployment tags for Elastic org"
  type = object({
    division = string
    org      = string
    team     = string
    project  = string
  })

  validation {
    condition = alltrue([
      for k, v in var.elastic_deployment_tags : trimspace(v) != ""
    ])
    error_message = "[ERROR] One of the required elastic deployment tags is missing"
  }
}