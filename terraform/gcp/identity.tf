resource "google_service_account" "main" {
  account_id   = "${var.deployment_prefix}-svc-acc"
  display_name = "Service Account for ${var.deployment_prefix} VM's"
}

data "google_compute_default_service_account" "default" {
}