data "google_compute_image" "winbox" {
  name    = local.winbox_specs.image_specs.image_name
  project = local.winbox_specs.image_specs.image_project
}

resource "google_compute_instance" "winbox" {
  count                     = var._ff_enable_winbox ? 1 : 0
  can_ip_forward            = true
  deletion_protection       = false
  enable_display            = false
  allow_stopping_for_update = true

  guest_accelerator = []
  labels            = local.default_labels
  machine_type      = local.winbox_specs.machine_type

  metadata = {
    "block-project-ssh-keys" = "true"
    "ssh-keys"               = "${var._ssh_username}:${local.ssh_pubkey}"
    "enable-windows-ssh"     = "TRUE"
  }

  name              = "${var.deployment_prefix}-winbox-vm"
  resource_policies = []
  hostname          = local.winbox_specs.hostname
  zone              = local.subnets.dmz.zone

  boot_disk {
    auto_delete = true

    initialize_params {
      image  = data.google_compute_image.winbox.self_link
      labels = local.default_labels
      size   = local.winbox_specs.root_disk_gb
      type   = "pd-ssd"
    }
  }

  network_interface {
    network    = google_compute_network.main.self_link
    stack_type = "IPV4_ONLY"
    subnetwork = google_compute_subnetwork.dmz.self_link

    access_config {
      nat_ip = google_compute_address.winbox.0.address
    }
  }

  service_account {
    email  = data.google_compute_default_service_account.default.email
    scopes = ["storage-rw"]
  }

  timeouts {}
}
