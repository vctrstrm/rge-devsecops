locals {
  ssh_privkey_path = abspath(pathexpand(var.ssh_key_path))
  ssh_pubkey       = trimspace(data.tls_public_key.main_ssh_key.public_key_openssh)
}

data "tls_public_key" "main_ssh_key" {
  private_key_openssh = sensitive(file(local.ssh_privkey_path))
}

# TODO: For future use...
data "google_client_openid_userinfo" "me" {

}

resource "google_os_login_ssh_public_key" "main" {
  user = data.google_client_openid_userinfo.me.email
  key  = local.ssh_pubkey
}