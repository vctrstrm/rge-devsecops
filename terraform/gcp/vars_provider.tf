#####
## ## These are variables having to do with provider setup,
## ##  - as such they could change with deployments.
#####

variable "gcp_project" {
  description = "Name of the GCP project for this deployment"
  type        = string
}
