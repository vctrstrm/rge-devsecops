data "google_compute_image" "linbox" {
  name    = local.linbox_specs.image_specs.image_name
  project = local.linbox_specs.image_specs.image_project
}

resource "google_compute_instance" "linbox" {
  can_ip_forward            = true
  deletion_protection       = false
  enable_display            = false
  allow_stopping_for_update = true

  guest_accelerator = []
  labels            = local.default_labels
  machine_type      = local.linbox_specs.machine_type

  metadata = {
    "block-project-ssh-keys" = "true"
    "ssh-keys"               = "${var._ssh_username}:${local.ssh_pubkey}"
    "user-data"              = data.cloudinit_config.linbox.rendered
  }

  name              = "${var.deployment_prefix}-linbox-vm"
  resource_policies = []
  hostname          = local.linbox_specs.hostname
  zone              = local.subnets.private.zone

  boot_disk {
    auto_delete = true

    initialize_params {
      image  = data.google_compute_image.linbox.self_link
      labels = local.default_labels
      size   = local.linbox_specs.root_disk_gb
      type   = "pd-ssd"
    }
  }

  network_interface {
    network    = google_compute_network.main.self_link
    stack_type = "IPV4_ONLY"
    subnetwork = google_compute_subnetwork.private.self_link
  }

  service_account {
    email  = data.google_compute_default_service_account.default.email
    scopes = ["storage-rw"]
  }

  timeouts {}
}

data "cloudinit_config" "linbox" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content = templatefile("${path.module}/templates/cloud-init.tpl", {
      ssh_pubkey        = local.ssh_pubkey
      ssh_user          = var._ssh_username
      instance_hostname = local.linbox_specs.hostname
    })
  }
}
