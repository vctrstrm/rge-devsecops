resource "google_dns_managed_zone" "private" {
  name        = "${var.deployment_prefix}-private-zone"
  dns_name    = "${local.deployment_domain}."
  description = "Example private DNS zone"
  labels      = local.default_labels

  visibility = "private"

  private_visibility_config {
    networks {
      network_url = google_compute_network.main.id
    }
  }
}

resource "google_dns_record_set" "bastion" {
  name = "bastion.${google_dns_managed_zone.private.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.private.name

  rrdatas = [google_compute_instance.bastion.network_interface.0.network_ip]
}

resource "google_dns_record_set" "linbox" {
  name = "linbox.${google_dns_managed_zone.private.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.private.name

  rrdatas = [google_compute_instance.linbox.network_interface.0.network_ip]
}

resource "google_dns_record_set" "winbox" {
  count = var._ff_enable_winbox ? 1 : 0
  name  = "winbox.${google_dns_managed_zone.private.dns_name}"
  type  = "A"
  ttl   = 300

  managed_zone = google_dns_managed_zone.private.name

  rrdatas = [google_compute_instance.winbox.0.network_interface.0.network_ip]
}

resource "google_dns_record_set" "wildcard" {
  name = "*.${google_dns_managed_zone.private.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.private.name

  rrdatas = [google_compute_instance.bastion.network_interface.0.network_ip]
}