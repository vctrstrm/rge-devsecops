#####
## ## Networks, Subnets, Gateways, Routers
#####

resource "google_compute_network" "main" {
  name                            = "${var.deployment_prefix}-vpc"
  auto_create_subnetworks         = false
  mtu                             = 1460
  routing_mode                    = "REGIONAL"
  delete_default_routes_on_create = false
}

resource "google_compute_router" "main" {
  name    = "${var.deployment_prefix}-crtr"
  region  = local.network.region
  network = google_compute_network.main.self_link
  bgp {
    asn = var._gcp_default_router_asn
  }
}

resource "google_compute_subnetwork" "dmz" {
  name          = "${var.deployment_prefix}-dmz-subnet"
  ip_cidr_range = local.subnets.dmz.cidr
  region        = local.network.region
  network       = google_compute_network.main.self_link

  private_ip_google_access = true
  stack_type               = "IPV4_ONLY"
}

resource "google_compute_subnetwork" "private" {
  name          = "${var.deployment_prefix}-private-subnet"
  ip_cidr_range = local.subnets.private.cidr
  region        = local.network.region
  network       = google_compute_network.main.self_link

  private_ip_google_access = true
  stack_type               = "IPV4_ONLY"
}

#####
## ## Stateful Firewall Rules
#####


resource "google_compute_firewall" "dmz" {
  for_each = { for idx, rule in local.firewall_rulesets["dmz"] : idx => rule }

  name = "${var.deployment_prefix}-dmz-fwrule-${each.key}"

  network   = google_compute_network.main.self_link
  direction = "INGRESS"
  priority  = each.value.priority

  allow {
    protocol = each.value.protocol
    ports    = each.value.ports
  }

  source_ranges = [each.value.source]
}

resource "google_compute_firewall" "private" {
  for_each = { for idx, rule in local.firewall_rulesets["private"] : idx => rule }

  name = "${var.deployment_prefix}-private-fwrule-${each.key}"

  network   = google_compute_network.main.self_link
  direction = "INGRESS"
  priority  = each.value.priority

  allow {
    protocol = each.value.protocol
    ports    = each.value.ports
  }

  source_ranges = [each.value.source]
}
