#####
## ## These are the variables that are overriden/changed in VERY RARE circumstances
## ##  - the idea is this allows some more in-depth deployment customization 
#####  - for the more adventurous (or unfortunate) of us.

variable "_map_deployment_location_to_details" {
  # whole list of em: https://cloud.google.com/compute/docs/regions-zones
  description = "Internal translation table for deployment locations to provider-specific network details"

  type = map(object({
    region = string
    zone   = string
  }))

  default = {
    "us-west" = {
      # OREGON
      region = "us-west2"
      zone   = "us-west2-b"
    },
    "us-east" = {
      # VIRGINIA
      region = "us-east4"
      zone   = "us-east4-a"
    },
    "us-central" = {
      # IOWA
      region = "us-central1"
      zone   = "us-central1-b"
    }
  }
}

variable "_map_machine_size_to_details" {
  # overview of machine types: https://cloud.google.com/compute/docs/machine-types
  # using cli to find various ones: https://cloud.google.com/sdk/gcloud/reference/compute/machine-types/list
  description = "Internal translation table for machine shortnames to provider-specific machine details"

  type = map(object({
    machine_type = string
  }))

  default = {
    "4c-16g" = {
      machine_type = "n2-standard-4"
    }
    "8c-32g" = {
      machine_type = "n2-standard-8"
    }
    "16c-64g" = {
      machine_type = "n2-standard-16"
    }
  }
}

variable "_map_os_name_to_image_details" {
  # you can use gcloud command to list em: https://cloud.google.com/sdk/gcloud/reference/compute/images/list
  description = "Internal translation table mapping OS shortnames to machine image details"

  type = map(object({
    image_name    = string
    image_project = string
  }))

  default = {
    "rhel-7-cis-stig" = {
      image_name    = "cis-red-hat-enterprise-linux-7-stig-v3-1-1-17"
      image_project = "cis-public"
    }
    "rhel-7" = {
      image_name    = "rhel-7-v20230203"
      image_project = "rhel-cloud"
    }
    "rhel-8-cis-l2" = {
      image_name    = "cis-red-hat-enterprise-linux-8-level-2-v2-0-0-5"
      image_project = "cis-public"
    }
    "rhel-8-cis-stig" = {
      image_name    = "cis-red-hat-enterprise-linux-8-stig-v1-0-0-5"
      image_project = "cis-public"
    }
    "rhel-8" = {
      image_name    = "rhel-8-v20230202"
      image_project = "rhel-cloud"
    }
    "rocky-8" = {
      image_name    = "rocky-linux-8-optimized-gcp-v20230202"
      image_project = "rocky-linux-cloud"
    }
    "winserv-2019" = {
      image_name    = "windows-server-2019-dc-v20230111"
      image_project = "windows-cloud"
    }
    "winserv-2019-cis-stig" = {
      image_name    = "cis-windows-server-2019-v1-1-0-8-stig"
      image_project = "cis-public"
    }
  }
}

variable "_firewall_rulesets" {
  description = "Sets of firewall rules by zone type"

  type = map(list(object({
    source    = string
    from_port = number
    to_port   = number
    protocol  = string
  })))

  default = {
    "dmz" = [
      # internal traffic
      { source = "internal", from_port = 0, to_port = 0, protocol = "all" },
      # ping!
      { source = "0.0.0.0/0", from_port = 0, to_port = 0, protocol = "icmp" },
      # ssh
      { source = "0.0.0.0/0", from_port = 22, to_port = 22, protocol = "tcp" },
      # http/https
      { source = "0.0.0.0/0", from_port = 443, to_port = 443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 80, to_port = 80, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 8443, to_port = 8443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 8080, to_port = 8080, protocol = "tcp" },
      # k8s api's, etc.
      { source = "0.0.0.0/0", from_port = 6443, to_port = 6443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 2379, to_port = 2380, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 9345, to_port = 9345, protocol = "tcp" },
      # K8s nodePort port range
      { source = "0.0.0.0/0", from_port = 30000, to_port = 32767, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 30000, to_port = 32767, protocol = "udp" },
      # Elastic Stack ports
      { source = "0.0.0.0/0", from_port = 5601, to_port = 5601, protocol = "tcp" },
      # RDP
      { source = "0.0.0.0/0", from_port = 3389, to_port = 3389, protocol = "tcp" }
    ]
    "private" = [
      { source = "internal", from_port = 0, to_port = 0, protocol = "all" }
    ]
  }
}

variable "_ansible_inventory_path" {
  description = "Ansible Inventory Path"
  type        = string
  default     = "../../generated/ansible/inventory/gcp.yml"
}

variable "_kubernetes_subdomains" {
  description = "List of kubernetes app subdomains"
  type        = set(string)
  default = [
    "k8s",
    "app",
    "apm",
    "es",
    "kb",
    "ent",
    "maps",
    "registry",
    "harbor",
    "kafka",
    "rancher",
    "rke2",
    "gitlab",
    "git",
    "bastion",
    "kas",
    "minio",
    "ems"
  ]
}

variable "_network_cidr" {
  description = "Deployment Network CIDR"
  type        = string
  default     = "10.0.0.0/16"
}

variable "_ssh_username" {
  description = "Default username for SSH purposes"
  type        = string
  default     = "elastic"
}

variable "_bastion_specs" {
  description = "Machine config for the bastion"

  type = object({
    machine_size = string
    root_disk_gb = number
    os_image     = string
  })

  default = {
    machine_size = "16c-64g"
    root_disk_gb = 800
    os_image     = "rhel-8"
  }
}

variable "_linbox_specs" {
  description = "Machine config for the linuxbox"

  type = object({
    machine_size = string
    root_disk_gb = number
    os_image     = string
  })

  default = {
    machine_size = "4c-16g"
    root_disk_gb = 400
    os_image     = "rhel-8"
  }
}

variable "_winbox_specs" {
  description = "Machine config for the windowsbox"

  type = object({
    machine_size = string
    root_disk_gb = number
    os_image     = string
  })

  default = {
    machine_size = "4c-16g"
    root_disk_gb = 400
    os_image     = "winserv-2019"
  }
}

variable "_gcp_default_router_asn" {
  description = "ASN for default cloud router"
  type        = number
  default     = 64514
}

## FEATURE FLAGS

variable "_ff_enable_winbox" {
  description = "[flag] enable winbox"
  type        = bool
  default     = false
}