resource "google_compute_address" "ingress" {
  name = "${var.deployment_prefix}-ingress-ip"
}

resource "google_compute_address" "winbox" {
  count = var._ff_enable_winbox ? 1 : 0
  name  = "${var.deployment_prefix}-winbox-ip"
}