locals {

  network = {
    region = lookup(
      var._map_deployment_location_to_details,
      var.deployment_location
    )["region"]
    zone = lookup(
      var._map_deployment_location_to_details,
      var.deployment_location
    )["zone"]
    cidr = var._network_cidr
  }

  subnets = {
    "dmz" = {
      cidr = cidrsubnet(local.network.cidr, 4, 0)
      zone = local.network.zone
    }
    "private" = {
      cidr = cidrsubnet(local.network.cidr, 4, 1)
      zone = local.network.zone
    }
  }

  # helper to translate some values
  _firewall_rule_translations = {
    protocol = {
      "all" = "all"
    }
    source = {
      "internal" = local.network.cidr
    }
  }

  firewall_rulesets = {
    for name, rules in var._firewall_rulesets :
    name => [
      for rule in rules : {
        ports = rule.from_port != 0 ? (
          rule.from_port != rule.to_port
          ? ["${rule.from_port}-${rule.to_port}"]
          : ["${rule.from_port}"]
        ) : []

        priority = 65534

        source = lookup(
          local._firewall_rule_translations.source,
          rule.source,
          rule.source
        )

        protocol = lookup(
          local._firewall_rule_translations.protocol,
          rule.protocol,
          rule.protocol
        )
      }
    ]
  }

  deployment_domain = "gcp.${var.deployment_prefix}.local"

  ingress_domains = concat(
    [local.deployment_domain],
    [
      for subdomain in var._kubernetes_subdomains :
      "${subdomain}.${local.deployment_domain}"
    ]
  )

  default_labels = merge({
    "owner"      = replace(lower(var.deployment_owner), "/[^a-z]+/", "-")
    "deployment" = var.deployment_prefix
  }, var.elastic_deployment_tags)

  bastion_specs = {
    hostname = "bastion.${local.deployment_domain}"
    machine_type = lookup(
      var._map_machine_size_to_details,
      var._bastion_specs.machine_size
    )["machine_type"]
    image_specs = lookup(
      var._map_os_name_to_image_details,
      var._bastion_specs.os_image
    )
    root_disk_gb = var._bastion_specs.root_disk_gb
  }

  linbox_specs = {
    hostname = "linbox.${local.deployment_domain}"
    machine_type = lookup(
      var._map_machine_size_to_details,
      var._linbox_specs.machine_size
    )["machine_type"]
    image_specs = lookup(
      var._map_os_name_to_image_details,
      var._linbox_specs.os_image
    )
    root_disk_gb = var._linbox_specs.root_disk_gb
  }

  winbox_specs = {
    hostname = "winbox.${local.deployment_domain}"
    machine_type = lookup(
      var._map_machine_size_to_details,
      var._winbox_specs.machine_size
    )["machine_type"]
    image_specs = lookup(
      var._map_os_name_to_image_details,
      var._winbox_specs.os_image
    )
    root_disk_gb = var._winbox_specs.root_disk_gb
  }

}

locals {
  bastion_public_ip  = google_compute_address.ingress.address
  bastion_private_ip = google_compute_instance.bastion.network_interface.0.network_ip
  linbox_private_ip  = google_compute_instance.linbox.network_interface.0.network_ip
  # TODO: [managed-k8s] set to load balancer if deploying managed k8s
  ingress_public_ip = var.deploy_managed_k8s ? null : local.bastion_public_ip

}

# winbox locals
locals {
  winbox_public_ip  = var._ff_enable_winbox ? google_compute_address.winbox.0.address : "1.1.1.1"
  winbox_private_ip = var._ff_enable_winbox ? google_compute_instance.winbox.0.network_interface.0.network_ip : "1.1.1.1"
}