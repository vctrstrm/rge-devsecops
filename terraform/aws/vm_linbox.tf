
data "aws_ami" "linbox" {
  most_recent = true

  owners = [local.linbox_specs.ami_details.image_owner_account_id]

  filter {
    name   = "name"
    values = [local.linbox_specs.ami_details.image_name_pattern]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_network_interface" "linbox" {
  subnet_id         = aws_subnet.private.id
  security_groups   = [aws_security_group.private.id]
  source_dest_check = false
  tags = {
    Name = "${var.deployment_prefix}-linbox-nic"
  }
}

resource "aws_instance" "linbox" {
  ami              = data.aws_ami.linbox.id
  instance_type    = local.linbox_specs.machine_type
  key_name         = aws_key_pair.main.key_name
  user_data_base64 = data.cloudinit_config.linbox.rendered

  tags = {
    Name = "${var.deployment_prefix}-linbox-vm"
  }

  network_interface {
    network_interface_id = aws_network_interface.linbox.id
    device_index         = 0
  }

  root_block_device {
    encrypted             = false
    delete_on_termination = true
    volume_type           = "gp3"
    volume_size           = local.linbox_specs.root_disk_gb
    iops                  = local.linbox_specs.iops
    throughput            = local.linbox_specs.throughput_mbps
    tags = {
      Name = "${var.deployment_prefix}-linbox-root-ebs"
    }
  }

  credit_specification {
    cpu_credits = "unlimited"
  }

  lifecycle {
    ignore_changes = [tags, tags_all]
  }
}

data "cloudinit_config" "linbox" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content = templatefile("${path.module}/templates/cloud-init.tpl", {
      ssh_pubkey        = local.ssh_pubkey
      ssh_user          = var._ssh_username
      instance_hostname = local.linbox_specs.hostname
    })
  }
}