#####
## ## These are the variables that are overriden/changed in VERY RARE circumstances
## ##  - the idea is this allows some more in-depth deployment customization 
#####  - for the more adventurous (or unfortunate) of us.

variable "_map_deployment_location_to_details" {
  # whole list of em: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
  description = "Internal translation table for deployment locations to provider-specific network details"

  type = map(object({
    region = string
    zone   = string
  }))

  default = {
    "us-west" = {
      # OREGON
      region = "us-west-2"
      zone   = "us-west-2a"
    },
    "us-east" = {
      # VIRGINIA
      region = "us-east-1"
      zone   = "us-east-1a"
    },
    "us-central" = {
      # OHIO
      region = "us-east-2"
      zone   = "us-east-2a"
    }
  }
}

variable "_map_machine_size_to_details" {
  # overview of machine types: https://aws.amazon.com/ec2/instance-types/
  # using cli to find various ones: https://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instance-types.html
  description = "Internal translation table for machine shortnames to provider-specific machine details"

  type = map(object({
    machine_type = string
  }))

  default = {
    "4c-16g" = {
      machine_type = "m6i.xlarge"
    }
    "8c-32g" = {
      machine_type = "m6i.2xlarge"
    }
    "16c-64g" = {
      machine_type = "m6i.4xlarge"
    }
  }
}

variable "_map_os_name_to_image_details" {
  # docs: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html
  description = "Internal translation table mapping OS shortnames to machine image details"

  type = map(object({
    image_name_pattern     = string
    image_owner_account_id = string
  }))

  default = {
    "rhel-8" = {
      image_name_pattern     = "RHEL-8.7*"
      image_owner_account_id = "309956199498"
    }
    "rocky-8" = {
      image_name_pattern     = "Rocky-8-ec2-8.7*"
      image_owner_account_id = "679593333241"
    }
    "rhel-8-stig" = {
      image_name_pattern     = "Nemu Hardened Computing RHEL8*"
      image_owner_account_id = "679593333241"
    }
    "rhel-7-stig" = {
      image_name_pattern     = "Nemu Hardened Computing RHEL7*"
      image_owner_account_id = "679593333241"
    }
    "winserv-2019-cis-stig" = {
      image_name_pattern     = "CIS Microsoft Windows Server 2019 STIG Benchmark*"
      image_owner_account_id = "679593333241"
    }
  }
}

variable "_firewall_rulesets" {
  # docs to understand relevant rule values: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
  description = "Sets of firewall rules by zone type"

  type = map(list(object({
    source    = string
    from_port = number
    to_port   = number
    protocol  = string
  })))

  default = {
    "dmz" = [
      # internal traffic
      { source = "internal", from_port = 0, to_port = 0, protocol = "all" },
      # ping!
      { source = "0.0.0.0/0", from_port = 0, to_port = 0, protocol = "icmp" },
      # ssh
      { source = "0.0.0.0/0", from_port = 22, to_port = 22, protocol = "tcp" },
      # http/https
      { source = "0.0.0.0/0", from_port = 443, to_port = 443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 80, to_port = 80, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 8443, to_port = 8443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 8080, to_port = 8080, protocol = "tcp" },
      # k8s api's, etc.
      { source = "0.0.0.0/0", from_port = 6443, to_port = 6443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 2379, to_port = 2380, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 9345, to_port = 9345, protocol = "tcp" },
      # K8s nodePort port range
      { source = "0.0.0.0/0", from_port = 30000, to_port = 32767, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 30000, to_port = 32767, protocol = "udp" }
    ]
    "private" = [
      { source = "internal", from_port = 0, to_port = 0, protocol = "all" }
    ]
  }
}

variable "_ansible_inventory_path" {
  description = "Ansible Inventory Path"
  type        = string
  default     = "../../generated/ansible/inventory/aws.yml"
}

variable "_kubernetes_subdomains" {
  description = "List of kubernetes app subdomains"
  type        = set(string)
  default = [
    "k8s",
    "app",
    "apm",
    "es",
    "kb",
    "ent",
    "maps",
    "registry",
    "harbor",
    "kafka",
    "rancher",
    "rke2",
    "gitlab",
    "git",
    "bastion",
    "kas",
    "minio",
    "ems"
  ]
}

variable "_network_cidr" {
  description = "Deployment Network CIDR"
  type        = string
  default     = "10.0.0.0/16"
}

variable "_ssh_username" {
  description = "Default username for SSH purposes"
  type        = string
  default     = "elastic"
}

variable "_bastion_specs" {
  description = "Machine config for the bastion"

  type = object({
    machine_size    = string
    root_disk_gb    = number
    os_image        = string
    iops            = number
    throughput_mbps = number
  })

  default = {
    machine_size    = "16c-64g"
    root_disk_gb    = 800
    os_image        = "rhel-8"
    iops            = 8000
    throughput_mbps = 800
  }
}

variable "_linbox_specs" {
  description = "Machine config for the linuxbox"

  type = object({
    machine_size    = string
    root_disk_gb    = number
    os_image        = string
    iops            = number
    throughput_mbps = number
  })

  default = {
    machine_size    = "4c-16g"
    root_disk_gb    = 400
    os_image        = "rhel-8"
    iops            = 4000
    throughput_mbps = 400
  }
}
