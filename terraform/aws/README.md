# So you want to use AWS?

**What to do first:**
- On MacOS, you'll need xcode-tools before you can install/compile some brew packages, e.g. `sudo xcode-select --install`
- Install Terraform CLI, e.g. using brew: `brew install terraform`
- Install AWS CLI, e.g. using brew: `brew install awscli`
- Configure AWS CLI with your AWS creds: e.g. `aws configure`
- Create `deployment.auto.tfvars` file in this directory, adjust contents appropriately to your deployment.

```ini
# deployment.auto.tfvars
deployment_owner    = "John Doe"
deployment_email    = "john.doe@elastic.co"
deployment_prefix   = "jdoe-eck-lab"
ssh_key_path        = "~/.ssh/id_rsa"
# valid locations are us-west, us-central, us-east
deployment_location = "us-west"

elastic_deployment_tags = {
  division = "field"
  org = "sa"
  team = "amer_fed_sa"
  project = "poc_and_demo_dev"
}

```

**Now you can launch:**
- Use terraform CLI to apply changes. E.g. `terraform apply`

