
resource "local_file" "ansible_inventory" {
  content = yamlencode({
    aws = {
      hosts = {
        bastion = {
          ansible_host       = local.bastion_public_ip
          public_ip_address  = local.bastion_public_ip
          private_ip_address = local.bastion_private_ip
          local_fqdn         = local.bastion_specs.hostname
        }
        linbox = {
          ansible_host            = local.linbox_private_ip
          private_ip_address      = local.linbox_private_ip
          ansible_ssh_common_args = "-J ${var._ssh_username}@${local.bastion_public_ip}"
          local_fqdn              = local.linbox_specs.hostname
        }
      }
      vars = {
        ansible_user                 = var._ssh_username
        ansible_ssh_private_key_file = local.ssh_privkey_path
        deployment_prefix            = var.deployment_prefix
        deployment_domain            = local.deployment_domain
        ingress_domains              = local.ingress_domains
        ingress_public_ip            = local.ingress_public_ip
      }
    }
  })
  filename        = pathexpand(var._ansible_inventory_path)
  file_permission = "0600"
}