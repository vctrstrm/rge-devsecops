locals {
  ssh_privkey_path = abspath(pathexpand(var.ssh_key_path))
  ssh_pubkey       = trimspace(data.tls_public_key.main_ssh_key.public_key_openssh)
}

data "tls_public_key" "main_ssh_key" {
  private_key_openssh = sensitive(file(local.ssh_privkey_path))
}

resource "aws_key_pair" "main" {
  key_name   = "${var.deployment_prefix}-main-ssh-key"
  public_key = local.ssh_pubkey
  tags = {
    Name = "${var.deployment_prefix}-main-ssh-key"
  }
}