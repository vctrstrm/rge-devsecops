resource "aws_route53_zone" "private" {
  name = local.deployment_domain

  vpc {
    vpc_id = aws_vpc.main.id
  }

  tags = {
    Name = "${var.deployment_prefix}-private-dns"
  }
}

resource "aws_route53_record" "bastion" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "bastion.${local.deployment_domain}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.bastion.private_ip]
}

resource "aws_route53_record" "wildcard" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "*.${local.deployment_domain}"
  type    = "A"
  ttl     = "300"
  # TODO: [managed-k8s] set to load balancer if deploying managed k8s
  records = [var.deploy_managed_k8s ? null : aws_instance.bastion.private_ip]
}

resource "aws_route53_record" "linbox" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "linbox.${local.deployment_domain}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.linbox.private_ip]
}