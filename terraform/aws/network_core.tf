#####
## ## Networks, Subnets, Gateways
#####

resource "aws_vpc" "main" {
  cidr_block           = local.network.cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true

  tags = {
    Name = "${var.deployment_prefix}-vpc"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.deployment_prefix}-main-igw"
  }
}

resource "aws_subnet" "private" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = local.subnets.private.cidr
  availability_zone = local.subnets.private.zone

  tags = {
    Name = "${var.deployment_prefix}-private-subnet"
  }

  map_public_ip_on_launch = false
  depends_on              = [aws_internet_gateway.main]
}

resource "aws_subnet" "dmz" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = local.subnets.dmz.cidr
  availability_zone = local.subnets.dmz.zone

  tags = {
    Name = "${var.deployment_prefix}-dmz-subnet"
  }

  map_public_ip_on_launch = false
  depends_on              = [aws_internet_gateway.main]
}

#####
## ## ROUTER SETTINGS
#####

resource "aws_default_route_table" "main" {
  default_route_table_id = aws_vpc.main.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = "${var.deployment_prefix}-default-rt"
  }
}

#####
## ## STATELESS FIREWALL SETTINGS (aka NACL's)
#####

resource "aws_default_network_acl" "main" {
  default_network_acl_id = aws_vpc.main.default_network_acl_id

  tags = {
    Name = "${var.deployment_prefix}-default-nacl"
  }

  # every subnet must have a NACL, so subnet_ids for default nacl tend to drift and since we don't care to manage the default nacl; we'll ignore these changes.
  lifecycle {
    ignore_changes = [subnet_ids]
  }
}

resource "aws_network_acl" "dmz_nacl" {
  vpc_id = aws_vpc.main.id

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "${var.deployment_prefix}-dmz-nacl"
  }
}

resource "aws_network_acl_association" "dmz_nacl_assoc" {

  network_acl_id = aws_network_acl.dmz_nacl.id
  subnet_id      = aws_subnet.dmz.id
}

resource "aws_network_acl" "private_nacl" {
  vpc_id = aws_vpc.main.id

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = local.network.cidr
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "${var.deployment_prefix}-private-nacl"
  }
}

resource "aws_network_acl_association" "private_nacl_assoc" {

  network_acl_id = aws_network_acl.private_nacl.id
  subnet_id      = aws_subnet.private.id
}



#####
## ## STATEFUL FIREWALL SETTINGS (aka Security Groups)
#####

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.deployment_prefix}-default-sg"
  }
}

resource "aws_security_group" "dmz" {
  name        = "${var.deployment_prefix}-dmz-sg"
  description = "DMZ Security Group"
  vpc_id      = aws_vpc.main.id

  tags = {
    Name = "${var.deployment_prefix}-dmz-sg"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  dynamic "ingress" {
    for_each = { for idx, rule in local.firewall_rulesets["dmz"] : idx => rule }

    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = [ingress.value.source]
    }

  }

}

resource "aws_security_group" "private" {
  name        = "${var.deployment_prefix}-private-sg"
  description = "Private Security Group"
  vpc_id      = aws_vpc.main.id

  tags = {
    Name = "${var.deployment_prefix}-private-sg"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  dynamic "ingress" {
    for_each = { for idx, rule in local.firewall_rulesets["private"] : idx => rule }

    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = [ingress.value.source]
    }
  }

}