locals {

  network = {
    region = lookup(
      var._map_deployment_location_to_details,
      var.deployment_location
    )["region"]
    zone = lookup(
      var._map_deployment_location_to_details,
      var.deployment_location
    )["zone"]
    cidr = var._network_cidr
  }

  subnets = {
    "dmz" = {
      cidr = cidrsubnet(local.network.cidr, 4, 0)
      zone = local.network.zone
    }
    "private" = {
      cidr = cidrsubnet(local.network.cidr, 4, 1)
      zone = local.network.zone
    }
  }

  # helper to translate some values
  _firewall_rule_translations = {
    protocol = {
      "all" = "-1"
    }
    source = {
      "internal" = local.network.cidr
    }
  }

  firewall_rulesets = {
    for name, rules in var._firewall_rulesets :
    name => [
      for rule in rules : {
        from_port = rule.from_port
        to_port   = rule.to_port

        source = lookup(
          local._firewall_rule_translations.source,
          rule.source,
          rule.source
        )

        protocol = lookup(
          local._firewall_rule_translations.protocol,
          rule.protocol,
          rule.protocol
        )

      }
    ]
  }

  deployment_domain = "aws.${var.deployment_prefix}.local"

  ingress_domains = concat(
    [local.deployment_domain],
    [
      for subdomain in var._kubernetes_subdomains :
      "${subdomain}.${local.deployment_domain}"
    ]
  )

  default_tags = merge({
    "owner"             = var.deployment_owner
    "deployment_prefix" = var.deployment_prefix
  }, var.elastic_deployment_tags)

  bastion_specs = {
    hostname = "bastion.${local.deployment_domain}"
    machine_type = lookup(
      var._map_machine_size_to_details,
      var._bastion_specs.machine_size
    )["machine_type"]
    ami_details = lookup(
      var._map_os_name_to_image_details,
      var._bastion_specs.os_image
    )
    root_disk_gb    = var._bastion_specs.root_disk_gb
    iops            = var._bastion_specs.iops
    throughput_mbps = var._bastion_specs.throughput_mbps
  }

  linbox_specs = {
    hostname = "linbox.${local.deployment_domain}"
    machine_type = lookup(
      var._map_machine_size_to_details,
      var._linbox_specs.machine_size
    )["machine_type"]
    ami_details = lookup(
      var._map_os_name_to_image_details,
      var._linbox_specs.os_image
    )
    root_disk_gb    = var._linbox_specs.root_disk_gb
    iops            = var._linbox_specs.iops
    throughput_mbps = var._linbox_specs.throughput_mbps
  }

}

locals {
  bastion_public_ip  = aws_eip.ingress.public_ip
  bastion_private_ip = aws_instance.bastion.private_ip
  linbox_private_ip  = aws_instance.linbox.private_ip
  # TODO: [managed-k8s] set to load balancer if deploying managed k8s
  ingress_public_ip = var.deploy_managed_k8s ? null : local.bastion_public_ip
}