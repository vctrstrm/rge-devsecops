#####
## ## These are variables having to do with provider setup,
## ##  - as such they could change with deployments.
#####

variable "aws_config_profile" {
  description = "Name of the AWS Config Profile to use for the API calls, typically found at ~/.aws/credentials"
  type        = string
  default     = "default"
}