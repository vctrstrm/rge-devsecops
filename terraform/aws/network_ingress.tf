#####
## ## APP INGRESS
#####

resource "aws_eip" "ingress" {
  vpc = true
  tags = {
    Name = "${var.deployment_prefix}-ingress-eip"
  }
}

# Routing straight to the bastion for now.
resource "aws_eip_association" "eip_assoc" {
  network_interface_id = aws_network_interface.bastion.id
  allocation_id        = aws_eip.ingress.id
}

# TODO: [managed-k8s] Use an NLB for ingress, with SSH to bastion and app traffic to k8s