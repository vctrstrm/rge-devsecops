# Terraform Infrastructure Setup Modules

## List of videos, from the main list at the beginning of the workshop
1. [Terraform in 100 seconds](https://youtu.be/tomUWcQ0P3k)


## Step 1: Create an alias for fast typing
Terraform is a long termto type. We are going to create an alias "tf" for faster typing

### Step 1.1: 
  In the terminal run this command 
  ```
  alias tf="terraform"
  ```

## Step 2: 
At the time of this writing, Terraform will be used to set up the workshop environment on top of Google Cloud

### Step 2.1
  Goto the gcp folder [here](./gcp)
