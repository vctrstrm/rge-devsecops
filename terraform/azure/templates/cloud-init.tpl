#cloud-config
preserve_hostname: false
hostname: ${instance_hostname}

cloud_final_modules:
- [users-groups,always]
users:
  - name: ${ssh_user}
    groups: [ wheel ]
    shell: /bin/bash
    sudo:
      - "ALL=(ALL) NOPASSWD:ALL"
    ssh-authorized-keys: 
      - ${ssh_pubkey}