locals {

  network = {
    region = lookup(
      var._map_deployment_location_to_details,
      var.deployment_location
    )["region"]
    zone = lookup(
      var._map_deployment_location_to_details,
      var.deployment_location
    )["zone"]
    cidr = var._network_cidr
  }

  subnets = {
    "dmz" = {
      cidr = cidrsubnet(local.network.cidr, 4, 0)
      zone = local.network.zone
    }
    "private" = {
      cidr = cidrsubnet(local.network.cidr, 4, 1)
      zone = local.network.zone
    }
  }

  # helper to translate some values
  _firewall_rule_translations = {
    protocol = {
      "tcp"  = "Tcp"
      "udp"  = "Udp"
      "icmp" = "Icmp"
      "all"  = "*"
    }
    source = {
      "internal" = local.network.cidr
    }
  }

  firewall_rulesets = {
    for name, rules in var._firewall_rulesets :
    name => [
      for idx, rule in rules : {

        priority  = 100 + (100 * idx)
        access    = "Allow"
        direction = "Inbound"

        source_port_range = "*"
        source_address_prefix = lookup(
          local._firewall_rule_translations.source,
          rule.source,
          rule.source
        )

        destination_port_range = rule.from_port != 0 ? (
          rule.from_port != rule.to_port
          ? "${rule.from_port}-${rule.to_port}"
          : "${rule.from_port}"
        ) : "*"
        destination_address_prefix = "*"

        protocol = lookup(
          local._firewall_rule_translations.protocol,
          rule.protocol,
          rule.protocol
        )
      }
    ]
  }

  deployment_domain = "azure.${var.deployment_prefix}.local"

  ingress_domains = concat(
    [local.deployment_domain],
    [
      for subdomain in var._kubernetes_subdomains :
      "${subdomain}.${local.deployment_domain}"
    ]
  )

  default_tags = merge({
    "owner"      = replace(lower(var.deployment_owner), "/[^a-z]+/", "-")
    "deployment" = var.deployment_prefix
  }, var.elastic_deployment_tags)

  bastion_specs = {
    hostname = "bastion.${local.deployment_domain}"
    machine_type = lookup(
      var._map_machine_size_to_details,
      var._bastion_specs.machine_size
    )["machine_type"]
    image_specs = lookup(
      var._map_os_name_to_image_details,
      var._bastion_specs.os_image
    )
    root_disk_gb      = var._bastion_specs.root_disk_gb
    root_storage_type = "Premium_LRS"
    root_caching      = "ReadWrite"
  }

  linbox_specs = {
    hostname = "linbox.${local.deployment_domain}"
    machine_type = lookup(
      var._map_machine_size_to_details,
      var._linbox_specs.machine_size
    )["machine_type"]
    image_specs = lookup(
      var._map_os_name_to_image_details,
      var._linbox_specs.os_image
    )
    root_disk_gb      = var._linbox_specs.root_disk_gb
    root_storage_type = "Premium_LRS"
    root_caching      = "ReadWrite"
  }

}

locals {
  bastion_public_ip  = azurerm_public_ip.ingress.ip_address
  bastion_private_ip = azurerm_network_interface.bastion.private_ip_address
  linbox_private_ip  = azurerm_network_interface.linbox.private_ip_address
  # TODO: [managed-k8s] set to load balancer if deploying managed k8s
  ingress_public_ip = var.deploy_managed_k8s ? null : local.bastion_public_ip
}