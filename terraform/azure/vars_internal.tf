#####
## ## These are the variables that are overriden/changed in VERY RARE circumstances
## ##  - the idea is this allows some more in-depth deployment customization 
#####  - for the more adventurous (or unfortunate) of us.

variable "_map_deployment_location_to_details" {
  # overview of Azure regions/zones: https://learn.microsoft.com/en-us/azure/availability-zones/az-overview
  # use CLI to list em out: "az account list-locations -o table"
  description = "Internal translation table for deployment locations to provider-specific network details"

  type = map(object({
    region = string
    zone   = string
  }))

  default = {
    "us-west" = {
      region = "westus2"
      zone   = "1"
    },
    "us-east" = {
      region = "eastus2"
      zone   = "1"
    },
    "us-central" = {
      region = "centralus"
      zone   = "1"
    }
  }
}

variable "_map_machine_size_to_details" {
  # overview of machine types: https://learn.microsoft.com/en-us/azure/virtual-machines/vm-naming-conventions
  # CLI: https://learn.microsoft.com/en-us/cli/azure/vm?view=azure-cli-latest#az-vm-list-sizes
  description = "Internal translation table for machine shortnames to provider-specific machine details"

  type = map(object({
    machine_type = string
  }))

  default = {
    "4c-16g" = {
      machine_type = "Standard_D4as_v5"
    }
    "8c-32g" = {
      machine_type = "Standard_D8as_v5"
    }
    "16c-64g" = {
      machine_type = "Standard_D16as_v5"
    }
  }
}

variable "_map_os_name_to_image_details" {
  # you can use az command to list em: https://learn.microsoft.com/en-us/cli/azure/vm/image?view=azure-cli-latest
  description = "Internal translation table mapping OS shortnames to machine image details"

  type = map(object({
    publisher = string
    offer     = string
    sku       = string
    version   = string
  }))

  default = {
    "rhel-8" = {
      publisher = "RedHat"
      offer     = "rhel-raw"
      sku       = "87-gen2"
      version   = "latest"
    }
    "rocky-8" = {
      publisher = "ciq"
      offer     = "rocky"
      sku       = "rocky-8-7-free"
      version   = "latest"
    }
    "rhel-8-cis-stig" = {
      publisher = "center-for-internet-security-inc"
      offer     = "cis-rhel-8-stig"
      sku       = "cis-rhel-8-stig"
      version   = "latest"
    }
    "winserv-2019-cis-stig" = {
      publisher = "center-for-internet-security-inc"
      offer     = "cis-win-2019-stig"
      sku       = "cis-win-2019-stig"
      version   = "latest"
    }
  }
}

variable "_firewall_rulesets" {
  description = "Sets of firewall rules by zone type"

  type = map(list(object({
    source    = string
    from_port = number
    to_port   = number
    protocol  = string
  })))

  default = {
    "dmz" = [
      # internal traffic
      { source = "internal", from_port = 0, to_port = 0, protocol = "all" },
      # ping!
      { source = "0.0.0.0/0", from_port = 0, to_port = 0, protocol = "icmp" },
      # ssh
      { source = "0.0.0.0/0", from_port = 22, to_port = 22, protocol = "tcp" },
      # http/https
      { source = "0.0.0.0/0", from_port = 443, to_port = 443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 80, to_port = 80, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 8443, to_port = 8443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 8080, to_port = 8080, protocol = "tcp" },
      # k8s api's, etc.
      { source = "0.0.0.0/0", from_port = 6443, to_port = 6443, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 2379, to_port = 2380, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 9345, to_port = 9345, protocol = "tcp" },
      # K8s nodePort port range
      { source = "0.0.0.0/0", from_port = 30000, to_port = 32767, protocol = "tcp" },
      { source = "0.0.0.0/0", from_port = 30000, to_port = 32767, protocol = "udp" }
    ]
    "private" = [
      { source = "internal", from_port = 0, to_port = 0, protocol = "all" }
    ]
  }
}

variable "_ansible_inventory_path" {
  description = "Ansible Inventory Path"
  type        = string
  default     = "../../generated/ansible/inventory/azure.yml"
}

variable "_kubernetes_subdomains" {
  description = "List of kubernetes app subdomains"
  type        = set(string)
  default = [
    "k8s",
    "app",
    "apm",
    "es",
    "kb",
    "ent",
    "maps",
    "registry",
    "harbor",
    "kafka",
    "rancher",
    "rke2",
    "gitlab",
    "git",
    "bastion",
    "kas",
    "minio",
    "ems"
  ]
}

variable "_network_cidr" {
  description = "Deployment Network CIDR"
  type        = string
  default     = "10.0.0.0/16"
}

variable "_ssh_username" {
  description = "Default username for SSH purposes"
  type        = string
  default     = "elastic"
}

variable "_bastion_specs" {
  description = "Machine config for the bastion"

  type = object({
    machine_size = string
    root_disk_gb = number
    os_image     = string
  })

  default = {
    machine_size = "16c-64g"
    root_disk_gb = 800
    os_image     = "rhel-8"
  }
}

variable "_linbox_specs" {
  description = "Machine config for the linuxbox"

  type = object({
    machine_size = string
    root_disk_gb = number
    os_image     = string
  })

  default = {
    machine_size = "4c-16g"
    root_disk_gb = 400
    os_image     = "rhel-8"
  }
}
