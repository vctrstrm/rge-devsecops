resource "azurerm_public_ip" "ingress" {
  name = "${var.deployment_prefix}-ingress-ip"

  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"

  tags = local.default_tags
}