resource "azurerm_private_dns_zone" "private" {
  name                = local.deployment_domain
  resource_group_name = azurerm_resource_group.rg.name

  tags = local.default_tags

  lifecycle {
    ignore_changes = [tags]
  }
}

resource "azurerm_private_dns_zone_virtual_network_link" "main" {
  name                  = "${var.deployment_prefix}-private-zone-link"
  resource_group_name   = azurerm_resource_group.rg.name
  private_dns_zone_name = azurerm_private_dns_zone.private.name
  virtual_network_id    = azurerm_virtual_network.main.id
}

resource "azurerm_private_dns_a_record" "bastion" {
  name                = "bastion"
  zone_name           = azurerm_private_dns_zone.private.name
  resource_group_name = azurerm_resource_group.rg.name
  ttl                 = 300
  records             = [local.bastion_private_ip]
}

resource "azurerm_private_dns_a_record" "linbox" {
  name                = "linbox"
  zone_name           = azurerm_private_dns_zone.private.name
  resource_group_name = azurerm_resource_group.rg.name
  ttl                 = 300
  records             = [local.linbox_private_ip]
}

resource "azurerm_private_dns_a_record" "wildcard" {
  name                = "*"
  zone_name           = azurerm_private_dns_zone.private.name
  resource_group_name = azurerm_resource_group.rg.name
  ttl                 = 300
  records             = [local.bastion_private_ip]
}