resource "azurerm_network_interface" "linbox" {
  name                 = "${var.deployment_prefix}-linbox-nic"
  location             = azurerm_resource_group.rg.location
  resource_group_name  = azurerm_resource_group.rg.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "${var.deployment_prefix}-linbox-ipconf"
    subnet_id                     = azurerm_subnet.private.id
    private_ip_address_allocation = "Dynamic"
  }

  tags = local.default_tags
}

resource "azurerm_linux_virtual_machine" "linbox" {
  name                = "${var.deployment_prefix}-linbox-vm"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = local.linbox_specs.machine_type

  disable_password_authentication = true
  user_data                       = data.cloudinit_config.linbox.rendered


  network_interface_ids = [
    azurerm_network_interface.linbox.id
  ]

  admin_username = var._ssh_username
  admin_ssh_key {
    username   = var._ssh_username
    public_key = local.ssh_pubkey
  }

  os_disk {
    caching              = local.linbox_specs.root_caching
    storage_account_type = local.linbox_specs.root_storage_type
    disk_size_gb         = local.linbox_specs.root_disk_gb
  }

  source_image_reference {
    publisher = local.linbox_specs.image_specs.publisher
    offer     = local.linbox_specs.image_specs.offer
    sku       = local.linbox_specs.image_specs.sku
    version   = local.linbox_specs.image_specs.version
  }

  tags = local.default_tags

  lifecycle {
    ignore_changes = [tags]
  }
}

data "cloudinit_config" "linbox" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content = templatefile("${path.module}/templates/cloud-init.tpl", {
      ssh_pubkey        = local.ssh_pubkey
      ssh_user          = var._ssh_username
      instance_hostname = local.linbox_specs.hostname
    })
  }
}