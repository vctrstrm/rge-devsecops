#####
## ## Networks, Subnets, Gateways, Routers
#####

resource "azurerm_virtual_network" "main" {
  name                = "${var.deployment_prefix}-vnet"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = [local.network.cidr]

  tags = local.default_tags
}

resource "azurerm_subnet" "dmz" {
  name                 = "${var.deployment_prefix}-dmz-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = [local.subnets.dmz.cidr]
}

resource "azurerm_subnet" "private" {
  name                 = "${var.deployment_prefix}-private-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = [local.subnets.private.cidr]
}

#####
## ## Stateful Firewall Rules
#####

resource "azurerm_network_security_group" "dmz" {
  name                = "${var.deployment_prefix}-dmz-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  tags = local.default_tags

  dynamic "security_rule" {
    for_each = { for idx, rule in local.firewall_rulesets["dmz"] : idx => rule }
    content {
      name                       = "${var.deployment_prefix}-dmz-fwrule-${security_rule.key}"
      priority                   = security_rule.value.priority
      direction                  = security_rule.value.direction
      access                     = security_rule.value.access
      protocol                   = security_rule.value.protocol
      source_port_range          = security_rule.value.source_port_range
      destination_port_range     = security_rule.value.destination_port_range
      source_address_prefix      = security_rule.value.source_address_prefix
      destination_address_prefix = security_rule.value.destination_address_prefix
    }
  }
}

resource "azurerm_subnet_network_security_group_association" "dmz" {
  subnet_id                 = azurerm_subnet.dmz.id
  network_security_group_id = azurerm_network_security_group.dmz.id
}

resource "azurerm_network_security_group" "private" {
  name                = "${var.deployment_prefix}-private-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  tags = local.default_tags

  dynamic "security_rule" {
    for_each = { for idx, rule in local.firewall_rulesets["private"] : idx => rule }
    content {
      name                       = "${var.deployment_prefix}-private-fwrule-${security_rule.key}"
      priority                   = security_rule.value.priority
      direction                  = security_rule.value.direction
      access                     = security_rule.value.access
      protocol                   = security_rule.value.protocol
      source_port_range          = security_rule.value.source_port_range
      destination_port_range     = security_rule.value.destination_port_range
      source_address_prefix      = security_rule.value.source_address_prefix
      destination_address_prefix = security_rule.value.destination_address_prefix
    }
  }
}

resource "azurerm_subnet_network_security_group_association" "private" {
  subnet_id                 = azurerm_subnet.private.id
  network_security_group_id = azurerm_network_security_group.private.id
}