resource "azurerm_network_interface" "bastion" {
  name                 = "${var.deployment_prefix}-bastion-nic"
  location             = azurerm_resource_group.rg.location
  resource_group_name  = azurerm_resource_group.rg.name
  enable_ip_forwarding = true

  ip_configuration {
    name                          = "${var.deployment_prefix}-bastion-ipconf"
    subnet_id                     = azurerm_subnet.dmz.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ingress.id
  }

  tags = local.default_tags
}

resource "azurerm_linux_virtual_machine" "bastion" {
  name                = "${var.deployment_prefix}-bastion-vm"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = local.bastion_specs.machine_type

  disable_password_authentication = true
  user_data                       = data.cloudinit_config.bastion.rendered


  network_interface_ids = [
    azurerm_network_interface.bastion.id
  ]

  admin_username = var._ssh_username
  admin_ssh_key {
    username   = var._ssh_username
    public_key = local.ssh_pubkey
  }

  os_disk {
    caching              = local.bastion_specs.root_caching
    storage_account_type = local.bastion_specs.root_storage_type
    disk_size_gb         = local.bastion_specs.root_disk_gb
  }

  source_image_reference {
    publisher = local.bastion_specs.image_specs.publisher
    offer     = local.bastion_specs.image_specs.offer
    sku       = local.bastion_specs.image_specs.sku
    version   = local.bastion_specs.image_specs.version
  }

  tags = local.default_tags

  lifecycle {
    ignore_changes = [tags]
  }
}

data "cloudinit_config" "bastion" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content = templatefile("${path.module}/templates/cloud-init.tpl", {
      ssh_pubkey        = local.ssh_pubkey
      ssh_user          = var._ssh_username
      instance_hostname = local.bastion_specs.hostname
    })
  }
}