# RGE DevSecOps
DevSecOps Platform with Rancher, Gitlab, & Elastic

## Prereqs
- OS: Linux, MacOSX
- Tools: ansible, terraform, kubectl, helm

## Organization
- `/terraform/<cloud>` - terraform code for setting up prerequisite vms in aws/gcp/azure
- `/ansible` - ansible playbooks for setting up kubernetes and local env
- `/k8s` - helm charts, manifests, and other things involved in creating deployments of elastic/gitlab/examples

## General Process (AWS Example)
- `cd /terraform/aws` then follow instructions in `/terraform/aws/README.md` to setup the deployment infrastructure.
- `cd /ansible` then follow `/ansible/README.md` to perform necessary local and remote env setup.
- `cd /k8s` then install ECK operator via `/k8s/deps/eck/setup.sh`
- `cd /k8s` then install Elastic stack helm chart at `/k8s/charts`
- additional post-setup: configure Rancher UI (guide-coming-soon)
- additional post-setup: install & configure Gitlab (guide-coming-soon)